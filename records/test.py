import cv2

def main():
    image = cv2.imread("misc/4.1.01.tiff",0)
    cv2.imshow('Linda', image)
    path = "saved/4.1.01.jpg"
    cv2.imwrite(path, image)
    cv2.waitKey(0)
    cv2.destroyWindow('Linda')
    
if __name__ == "__main__":
    main()