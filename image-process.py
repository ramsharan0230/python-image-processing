import cv2
import numpy as np
from matplotlib import pyplot as plt

def main():
    image = cv2.imread('misc/4.1.01.tiff', 1)
    cv2.imshow("Messi", image)
    print(type(image))
    print(image.dtype)
    print(image.shape)
    print(image.ndim)
    print(image.size) #its the multiple of all three dimensions
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    
if __name__ == "__main__":
    main()