import csv
from matplotlib import pyplot as plt
import numpy as np
from collections import Counter 


plt.style.use('fivethirtyeight')

with open('/records/SalesJan.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    country_counter = Counter()
    
    for row in csv_reader:
        country_counter.update(row['Country'].split(','))
    
    countries = []
    invlvmt = []
    
    for item in country_counter.most_common(10):
        countries.append(item[0])
        invlvmt.append(item[1])
    
    countries.reverse()
    invlvmt.reverse()   
    plt.barh(countries, invlvmt)
    plt.title("Popularity Index:")
    plt.ylabel("Year")
    plt.xlabel("Countries")
    plt.tight_layout()
    plt.show()