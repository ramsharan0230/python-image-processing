import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 
from datetime import datetime, timedelta
from matplotlib import dates as mpl_dates

plt.style.use('seaborn')

dates = [
    datetime(2019, 2, 23),
    datetime(2018, 3, 20),
    datetime(2017, 8, 3),
    datetime(2016, 7, 10),
    datetime(2015, 3, 16),
    datetime(2014, 11, 2),
    datetime(2013, 2, 9),
    datetime(2012, 12, 25)
]
dates.reverse();
plt.gcf().autofmt_xdate()
date_format = mpl_dates.DateFormatter('%b, %d, %Y')
plt.gca().xaxis.set_major_formatter(date_format)
y = [1,2,3,4,5, 6, 7, 8]
plt.plot_date(dates, y, linestyle='solid')
plt.show()