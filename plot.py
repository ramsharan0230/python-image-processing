from matplotlib import pyplot as plt
# print(plt.style.available)
plt.xkcd();
plt.style.use('ggplot')
dev_x = [20,25,30,35,40]
dev_AllDev = [15000,25000,20000,100000,150000]
dev_Python = [25000,30000,45000,65000,150000]
dev_Js = [13000,34000,40000,65000,90000]
plt.plot(dev_x, dev_AllDev, 'k--', linewidth="3", marker=".", label='All Developers')
plt.plot(dev_x, dev_Python, 'b', marker="*",linewidth="3", label='Python Developers')
plt.plot(dev_x, dev_Js, color='#444444', marker=".",linewidth="3", label='Js Developers')
plt.title("Salary Of Neplease Youth")
plt.xlabel("Ages")
plt.ylabel("Salary in Avarage")
plt.legend()
plt.grid(True)
plt.savefig('plot.png')
plt.show()