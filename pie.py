from matplotlib import pyplot as plt

plt.style.use('fivethirtyeight')

slices = [45, 55, 43,35, 30]
maximum = max(slices)
data = sorted(slices, reverse=True)
print(data)
labels = ['Python', 'Javascript', 'Java', 'PHP', 'Others']
explode = [0.05,0,0,0, 0]
plt.pie(slices, autopct='%1.1f%%', labels=labels, explode=explode, startangle=90,wedgeprops={'edgecolor':'black'})
plt.show()