from matplotlib import pyplot as plt
import numpy as np

x = [1, 2, 3, 4, 5]
player1 = [1, 1, 2, 3, 5]
player2 = [0, 4, 2, 6, 8]
player3 = [1, 3, 5, 7, 9]

labels = ["Fibonacci ", "Evens", "Odds"]
colors = ['#325aa8','#f00736','#16ba47']

plt.stackplot(x, player1, player2, player3,colors=colors, labels=labels)
plt.legend(loc='upper left')
plt.title('Players and Their Performances')
plt.xlabel('Time(in Minutes)')
plt.ylabel('Scores')
plt.show()

blue = '#325aa8'
purple = '#325aa8'
red = '#f00736'
black = '#171617'
green = '#16ba47'